import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../components/Home'
import UserList from '../components/UserList'
import UserDetail from '../components/UserDetail'

Vue.use(VueRouter)

export default new VueRouter({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },

    {
      path: '/users',
      name: 'UserList',
      component: UserList,
    },
    {
      path: '/users/:id',
      name: 'UserDetail',
      component: UserDetail
    },
  ]
})
export default {
  SET_LOADING (state, data) {
    state.loading = data
  },

  FETCH_USERS (state, data) {
    state.items = data
  },

  DELETE_RANDOM_USER (state, value) {
    const users = state.items
    users.data.splice( value, 1);
    state.items = users
  }
}

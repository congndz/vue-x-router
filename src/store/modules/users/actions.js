import axios from "axios";

export default {
  async getAllUsers({commit}) {
    commit('SET_LOADING', true);
    const url = 'https://jsonplaceholder.typicode.com/users'
    await axios.get(url).then(function (response) {
      commit('FETCH_USERS', response);
    }).catch(function (error) {
      console.log('Err', error);
    });
    commit('SET_LOADING', false)
  },

  randomDelete({commit}, value) {
    commit('DELETE_RANDOM_USER', value);
  }
}
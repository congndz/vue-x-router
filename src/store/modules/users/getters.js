export default {
  doneUsers: state => {
    const data = state.items.data
    if (data) {
      return data.filter(user => user.id % 2 === 0)
    }
  }
}